var express = require("express");
var bodyParser = require("body-parser");

var jsforce = require("jsforce");


var app = express();
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use('/slds', express.static(__dirname + '/node_modules/@salesforce-ux/design-system/assets'));


// Connection
var jsforce = require('jsforce');
var conn = new jsforce.Connection({
  // you can change loginUrl to connect to sandbox or prerelease env.
   loginUrl : 'https://test.salesforce.com'
});


// Initialize the app.
var server = app.listen(process.env.PORT || 8080, function () {
  var port = server.address().port;
  console.log("App now running on port", port);
});

// CONTACTS API ROUTES BELOW

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}

/*  "/contacts"
 *    GET: finds all accounts
 *    POST: creates a new account
 */

app.get("/contacts", function(req, res) {

conn.login('oceadmin@oceada.com.qa', 'Iqvia12!', function(err, userInfo) {
  if (err) { return console.error(err); }
  else{
    conn.query("select id,name from account where lastname != 'CPTEST' limit 100", function(err, result) {
    if (err) { return console.error(err); }
    if(result.records && result.records.length){
        console.log(result.records);
          res.send(result.records);
        }
  });
  }
   });
});


app.post("/contacts", function(req, res) {
  var newContact = req.body;
  console.log(req.body);

  if (!(req.body.lastname)) {
    handleError(res, "Invalid user input", "Must provide a last name.", 400);
  }

    // Single record creation
    conn.sobject('Account').create(
      req.body,
      function (err, resp) {
        if (err || !resp.success) {
          return console.error(err, resp)
        }
        console.log('Created record id : ' + resp.id)
        res.send(resp);
      }
    )
  });



/*  "/contacts/:id"
 *    GET: find contact by id
 *    PUT: update contact by id
 *    DELETE: deletes contact by id
 */

app.get("/contacts/:id", function(req, res) {
  console.log('attempting to get the contact');
  // Single record retrieval

  conn.chatter.resource('/services/data/v42.0/ui-api/record-ui/'+req.params.id).retrieve(function(err, resp) {
    
    if (err) { return console.error(err); }

    res.send(resp);
    
  });

});
